import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { User } from '../models/user'
import { ProfileService } from '../services/profile.service' 

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
	users: any;
	name: string = localStorage.getItem("name");
	balance: string = localStorage.getItem("balance");
	mobile: string = localStorage.getItem("mobile");
	id: string = localStorage.getItem("id");
  constructor(private router:Router, private profileService:ProfileService ) { }

  ngOnInit(): void {
		const token:string = localStorage.getItem("token");
		if(!token){
			this.router.navigate(['/login']);
		}
		this.getUsers();
	}

	logout(){
		localStorage.clear();
		this.router.navigate(['/login']);
	}
	
	getUsers(){
		this.profileService.getUsers().subscribe((res) =>{
			this.users = res
			this.users.users.forEach(element => {
				element.tBalance = 0
			});
		},
		(err)=>{
			console.log(err)
		})
	}
	transfer(toUserID:number,balance:number){
		this.profileService.balanceTransfer(parseInt(this.id), toUserID, balance).subscribe((res) =>{
			console.log("res", res)
			this.getUsers();
			this.profileService.getBalance(this.id).subscribe((res)=>{
				this.balance = res.balance
			})
		},
		(err)=>{
			console.log(err)
		})
	}

}

