import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RegisterUserComponent } from './register-user/register-user.component';
import { ProfileComponent } from './profile/profile.component'
import { TranactionsComponent } from './tranactions/tranactions.component'
// import { AuthGuard } from './auth/guards/auth.guard';
// import { RandomGuard } from './auth/guards/random.guard';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: '/login' },
  {
    path: 'login',
    component: RegisterUserComponent,
    // canActivate: [AuthGuard]
	},
	{
    path: 'profile',
    component: ProfileComponent,
    // canActivate: [AuthGuard]
	},
	{
		path: 'transactions',
		component: TranactionsComponent
	}
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule],
  declarations: []
})
export class AppRoutingModule { }