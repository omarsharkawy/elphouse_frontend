import { Injectable } from '@angular/core';
import { User } from '../models/user';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TransactionsService {
	getUsersUrl:string = 'http://localhost:3000/api/users'
	constructor(private http: HttpClient) { }

	getUsers(): Observable<User>{
		return this.http.get<User>(this.getUsersUrl)
	}

	
}
