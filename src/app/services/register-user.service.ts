import { Injectable } from '@angular/core';
import { User } from '../models/user';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RegisterUserService {
	loginUrl:string = 'http://localhost:3000/api/sessions'
	signupUrl:string = 'http://localhost:3000/api/users'
	constructor(private http: HttpClient) { }

	login(mobile, password): Observable<User>{
		return this.http.post<User>(this.loginUrl,{mobile, password})
	}

	signup(name, mobile, password): Observable<User>{
		return this.http.post<User>(this.signupUrl,{name, mobile, password}, )
	}
	
}
