import { Injectable } from '@angular/core';
import { User } from '../models/user';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProfileService {
	getUsersUrl:string = 'http://localhost:3000/api/users'
	getBalanceUrl:string = 'http://localhost:3000/api/users/balance'
	constructor(private http: HttpClient) { }

	getUsers(): Observable<any>{
		// const headerDict = {
		// 	'Content-Type': 'application/json',
		// 	'Accept': 'application/json',
		// 	'Access-Control-Allow-Headers': 'Content-Type',
		// 	 Authorization: "Bearer " + localStorage.getItem("token")
		// }
		let headers = new HttpHeaders();
		headers.append('Content-Type', 'application/json')
		headers.append('Accept', 'application/json')
		headers.append('Access-Control-Allow-Headers', 'Content-Type')
		headers.append('Authorization',"Bearer " + localStorage.getItem("token"));
		// this.header = new Headers({ Authorization: "Bearer " + localStorage.getItem("token")});
		return this.http.get<any>(this.getUsersUrl, {headers: headers})
	}
	balanceTransfer(from_user_id, to_user_id, balance_value): Observable<any>{
		return this.http.put<any>(this.getUsersUrl, {from_user_id, to_user_id, balance_value})
	}

	getBalance(user_id): Observable<any>{
		return this.http.post<any>(this.getBalanceUrl, {user_id})
	}

	
}
