export class User{
	id?:number;
	name: string;
	mobile: number;
	token?: string;
	balance?: number;
}