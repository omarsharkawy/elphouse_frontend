import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { RegisterUserService } from '../services/register-user.service'
import { transition } from '@angular/animations';
import { Router } from "@angular/router";



@Component({
  selector: 'app-register-user',
  templateUrl: './register-user.component.html',
  styleUrls: ['./register-user.component.scss']
})



export class RegisterUserComponent implements OnInit {
	mobile: string;
	password: string;
	error:boolean;
	mobileSignup: string;
	passwordSignup: string;
	nameSignup: string;

	
	constructor(private registerUserService: RegisterUserService
		,private router:Router
		) { }
	
	login(){
		this.registerUserService.login(this.mobile,this.password).subscribe((res) =>{
			localStorage.setItem("token",res.token);
			localStorage.setItem("name",res.name);
			localStorage.setItem("id",res.id+"");
			localStorage.setItem("balance",res.balance+"");
			localStorage.setItem("mobile",res.mobile+"");
			if(res.mobile+"" == "admin"){
				this.router.navigate(['/transactions']);
			}
			else{
				this.router.navigate(['/profile']);
			}
			
		},
		(err)=>{
			this.error=true;
		})
	}

	signup(){
		this.registerUserService.signup(this.nameSignup, this.mobileSignup,this.passwordSignup).subscribe((res) =>{
			localStorage.setItem("token",res.token);
			localStorage.setItem("name",res.name);
			localStorage.setItem("id",res.id+"");
			localStorage.setItem("balance",res.balance+"");
			localStorage.setItem("mobile",res.mobile+"");
			if(res.mobile+"" == "admin"){
				this.router.navigate(['/transactions']);
			}
			else{
				this.router.navigate(['/profile']);
			}
			
		},
		(err)=>{
			this.error=true;
		})
	}

  ngOnInit(): void {
	}
	

}
